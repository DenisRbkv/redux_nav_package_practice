import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:simple_login/src/app_store.dart';
import 'package:simple_login/src/login/login_bloc.dart';

import '../main_layout.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, LoginBloc>(
      converter: (store) {
        return LoginBloc(store);
      },
      builder: (BuildContext context, LoginBloc bloc) {
        return MainLayout(
            drawerFunc: bloc.login,
            buttonTitle: 'Go to page 2',
            appBar: 'Page 1',
            child: Builder(
              builder: (BuildContext context) {
                return Center(
                  child: Text('Page 1'),
                );
              },
            ));
      },
    );
  }
}
