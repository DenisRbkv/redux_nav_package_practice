import 'package:flutter/material.dart';

class DrawerHead extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      width: double.infinity,
      color: Theme.of(context).primaryColor,
      child: Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.only(left: 25.0, top: 15.0),
          child: Row(
            children: <Widget>[
              Icon(
                Icons.chrome_reader_mode,
                size: 40.0,
                color: Theme.of(context).secondaryHeaderColor,
              ),
              SizedBox(width: 20.0),
              Text('Redux Nav Drawer', style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.w500)),
            ],
          ),
        ),
      ),
    );
  }
}
