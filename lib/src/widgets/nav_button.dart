import 'package:flutter/material.dart';

class NavButton extends StatelessWidget {
  final Function _changePage;
  final String _title;

  NavButton(this._changePage, this._title);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black38,
            blurRadius: 10.0,
            spreadRadius: 0.0,
            offset: Offset(
              4.0, // horizontal
              3.0, // vertical
            ),
          ),
        ],
      ),
      margin: const EdgeInsets.only(top: 8.0, left: 10.0, bottom: 8.0),
      height: 45,
      width: 220,
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: _changePage,
          splashColor: Colors.white,
          borderRadius: BorderRadius.circular(10),
          child: Center(
            child: Text(
              _title,
              style: TextStyle(
                fontSize: 20,
                color: Theme.of(context).secondaryHeaderColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
