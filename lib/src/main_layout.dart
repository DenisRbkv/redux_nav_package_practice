import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'widgets/nav_button.dart';
import 'widgets/drawer_head.dart';

class MainLayout extends StatelessWidget {
  final String appBar;
  final Widget child;
  final Function drawerFunc;
  final String buttonTitle;


  MainLayout({@required this.appBar, @required this.child, @required this.drawerFunc, @required this.buttonTitle});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: child,
      appBar: AppBar(
        title: Text(appBar),
      ),
      drawer: Drawer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            DrawerHead(),
            Padding(
              padding: const EdgeInsets.only(left: 13.0, top: 10.0),
              child: NavButton(drawerFunc, buttonTitle),
            ),
          ],
        ),
      ),
    );
  }
}
